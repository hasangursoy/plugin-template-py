import json
import lib.node as node


class Get(node.Node):
    class Variable:
        def __init__(self, scope='', name=''):
            self.scope = scope
            self.name = name

    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.variable = variable

    def __init__(self):
        self.node_name = 'Plugin.Clipboard.Get'
        self.props = self.NodeProps(self.Variable())

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, config):
        self.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                               config['variable'])
        # TODO
        return

    async def on_message_cb(self, data):
        # TODO
        msg = json.loads(data)
        msg['payload'] = 'py plugin'
        return json.dumps(msg).encode()

    def on_close_cb(self):
        # TODO
        return
