class INodeFactory:
    def on_create(self, config):
        raise NotImplementedError('The method is not implemented.')


class NodeFactoryHandler:
    def __init__(self, handler):
        self.handler = handler

    async def handler_func(self, msg):
        await self.handler.on_create(msg.data)
