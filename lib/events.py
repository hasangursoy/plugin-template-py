from enum import Enum
import json
from json import JSONEncoder
import time
import lib.log as log
import lib.messaging as messaging
import sys


class EType(Enum):
    CREATE = 0,
    INPUT = 1,
    OUTPUT = 2,
    ERROR = 3,
    CLOSE = 4,
    LOG = 5,
    FLOW = 6,
    DEBUG = 7


class EventData:
    def __init__(self, message_id, e_type, node_id, payload, timestamp):
        self.messageId = message_id
        self.type = e_type
        self.nodeid = node_id
        self.payload = payload
        self.timestamp = timestamp

    def __repr__(self):
        return json.dumps(self.__dict__)

		
async def emit_error(guid, error):
#    print('[ERROR] ' + guid + '.' + error["message"])
#    sys.stdout.flush()
#    print("1")
    msg = json.loads(error["payload"])
    msg["error"] = {
        "type":"plugin",
        "message":error["message"],
        "source": {
            "id":error["guid"],
            "name":error["name"]
        }
    }
    ed = {
        "payload":msg
    }

    log.error(error["message"])
#    print("2")
    data = json.dumps(ed).encode()
    await messaging.send('Error.'+guid,  data)
#    print("3")
    e = EventData(msg["id"], 'error', guid, json.dumps(msg), int(time.time_ns()/10**6))
#    print("3.5", e)
    data = e.__repr__().encode()
    #data = json.dumps(e).encode()
#    print("4")
    await emit_event(EType.ERROR, guid, data)
#    print("5")


async def emit_flow_event(guid, name):
    e = EventData('', 'event', guid, name, int(time.time_ns()/10**6))
    data = e.__repr__().replace('\'', '\"').encode()
    #data =  json.dumps(e).encode()
    await emit_event(EType.FLOW, guid, data)


async def emit_output(guid, output, port):
    node_map = sys.modules.get('__main__').__dict__['rob'].nodeMap
    if node_map.keys().__contains__(guid):
        wires = node_map[guid]
        for wire in wires[port]:
            await messaging.send(wire+'.OnMessage', output)

        await emit_event(EType.OUTPUT, guid, output)


async def emit_event(e_type, subject, data):
    node_map = sys.modules.get('__main__').__dict__['rob'].nodeMap
    if e_type == EType.CREATE:
        await messaging.send(subject, data)
    elif e_type == EType.INPUT:
        await messaging.send(subject+'.OnMessage', data);
    elif e_type == EType.OUTPUT:
        await emit_flow_event(subject, 'send')
    elif e_type == EType.FLOW:
        await messaging.send('FlowEvent', data)
    # elif EType.DEBUG:
    # elif EType.LOG:
    elif EType.ERROR:
        await messaging.send('FlowEvent', data)
        await emit_flow_event(subject, 'error')
    elif EType.CLOSE:
        for guid in node_map.keys():
            await messaging.send(guid+'.OnClose', [])

        node_map.clear()


class Encoder(JSONEncoder):
    def default(self, obj):
        return obj.__dict__
