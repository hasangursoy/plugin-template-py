import sys

async def connection():
    conn = sys.modules['__main__'].__dict__['rob'].connection
    if not conn.is_connected:
        await conn.connect()

    return conn


async def send(subject, data):
    conn = await connection()
    await conn.publish(subject, data)
    await conn.flush(5)