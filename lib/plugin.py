import asyncio
import json
import sys
import os
import lib.log as log

cwd = os.getcwd()
path = os.path.realpath(__file__)
path = path.replace(os.path.join('lib', os.path.basename(__file__)), '')
path = path.replace(cwd, '')
if len(path) > 0:
    path = path[1:-1]
sys.path.insert(1, os.path.join(path, 'venv', 'Lib', 'site-packages'))

from nats.aio.client import Client as NATS

nodeMap = {}
connection = NATS()
activeNodes = 0


async def run():	
    await connection.connect()
    await connection.subscribe('wires', cb=wire_handler)

    nFactories = sys.modules.get('__main__').nFactories
    for fact in nFactories:
        await fact.register()
	

    log.info("Plugin initialized", status="started")

    event = asyncio.Event()	
    await event.wait()
    #future = asyncio.Future()
    #await asyncio.wait_for(future, timeout=None)


def wire_handler(msg):
    if len(msg.data) == 0:
        nodeMap.clear()
    else:
        flow = json.loads(msg.data)
        nodeMap[flow['Guid']] = flow['Wires']


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())
    loop.close()
