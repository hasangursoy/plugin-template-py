from datetime import datetime, timezone
import sys
import json

def info(msg, **kwargs):
    log("info", msg, **kwargs)

def error(msg, **kwargs):
    log("error", msg, **kwargs)

def warning(msg, **kwargs):
    log("warning", msg, **kwargs)

def debug(msg, **kwargs):
    log("debug", msg, **kwargs)

def log(level, msg, **kwargs):
    lt = datetime.now(timezone.utc).replace(microsecond=0).astimezone()

    l = {
        "level": level,
        "msg": msg,
        "time":lt.isoformat()
    }
    for key in kwargs:
        l[key] = kwargs[key]

    print(json.dumps(l))
    sys.stdout.flush()


