import lib.factory as factory
import lib.messaging as messaging
import lib.node as node
import lib.log as log
import json
from nats.aio.errors import ErrTimeout


async def register_node(name, fact):
    connection = await messaging.connection()
    nf = factory.NodeFactoryHandler(fact)
    await connection.subscribe(name, cb=nf.handler_func)


async def create_node(guid, name, a_node):
    connection = await messaging.connection()
    n = node.NodeHandler(guid, name, handler=a_node)
    n.subscription = await connection.subscribe(n.subject(), cb=n.handler_func)

async def getVaultItem(vaultId, itemId):
    try:
        conn = await messaging.connection()
        req = {
            "vaultId": vaultId,
            "itemId": itemId
        }
        response = await conn.request("VaultRequests", json.dumps(req).encode(), timeout=30)
        return json.loads(response.data.decode())
    except ErrTimeout:
        log.error("get vault item timedout")
    return {}
