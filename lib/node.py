import json
import sys
import time

import lib.events as events
import lib.factory as factory
import lib.messaging as messaging
import lib.plugin as plugin
import lib.log as log
import lib.runtime as runtime



class INode:
    async def on_message(self, in_msg):
        raise NotImplementedError('Method is not implemented.')

    def on_close(self):
        raise NotImplementedError('Method is not implemented.')


class NodeHandler:
    def __init__(self, guid='', name='', subscription=None, handler=None):
        self.guid = guid
        self.name = name
        self.subscription = subscription
        self.handler = handler

    def subject(self):
        return self.guid+'.'+'>'

    async def handler_func(self, msg):
        guid = msg.subject.split('.')[0]
        method = msg.subject.split('.')[1]

        def check_guid():
            nonlocal guid
            if guid == '':
                guid = self.guid

        check_guid()
        resp=[]

        if method == 'OnClose':
            log.info(self.name+' on close')
            sys.stdout.flush()
            connection = await messaging.connection()
            await connection.unsubscribe(self.subscription)
            self.handler.on_close()
        elif method == "OnMessage":
            await events.emit_flow_event(guid, 'receive')
            async def on_msg():
                nonlocal resp
                log.info(self.name+' on message started')
                sys.stdout.flush()
                resp = await self.handler.on_message(msg.data)
            try:
                await on_msg()
                log.info(self.name+' on message finished')
                sys.stdout.flush()
                await events.emit_output(guid, resp, 0)
            except BaseException as error:
                err = {
                    "message": str(error),
                    "payload": msg.data,
                    "guid":self.guid,
                    "name":self.name,
                }
                await events.emit_error(guid, err)


class Variable:
    def __init__(self, scope, name):
        self.scope = scope
        self.name = name


class INodeProps:
    def __init__(self, guid="", name="", delay_before=0, delay_after=0):
        self.guid = guid
        self.name = name
        self.delay_before = delay_before
        self.delay_after = delay_after


class Node(INode):
    def __init__(self, props):
        self.props = props

    def on_create_cb(self, config):
        raise NotImplementedError('The method is not implemented.')

    def on_message_cb(self, data):
        raise NotImplementedError('The method is not implemented.')

    def on_close_cb(self):
        raise NotImplementedError('The method is not implemented.')

    async def on_message(self, data):
        time.sleep(self.props.delay_before)
        try:
            outMsg = await self.on_message_cb(data)
            time.sleep(self.props.delay_after)
            return outMsg
        except BaseException as error:
            raise error

        return None

    def on_close(self):
        self.on_close_cb()
		
        sys.modules['__main__'].__dict__['rob'].activeNodes -= 1
        if sys.modules['__main__'].__dict__['rob'].activeNodes == 0:
            raise SystemExit
        return None

			
class NodeFactory(factory.INodeFactory):
    def __init__(self, node, node_name):
        self.node = node
        self.node_name = node_name
        sys.modules.get('__main__').nFactories.append(self)


    async def register(self):
        await runtime.register_node(self.node_name, self)


    async def on_create(self, config):
        json_string = json.loads(config)
        sys.modules['__main__'].__dict__['rob'].activeNodes += 1

        if 'delayBefore' not in json_string:
            json_string['delayBefore'] = 0
        if 'delayAfter' not in json_string:
            json_string['delayAfter'] = 0

        self.node.props = INodeProps(json_string['guid'], json_string['name'],
                             json_string['delayBefore'], json_string['delayAfter'])
        #n = Node(json_string['guid'], json_string['name'], json_string['variable'],
        #                     json_string['delayBefore'], json_string['delayAfter'])

        self.node.on_create_cb(json_string)
        await runtime.create_node(self.node.props.guid, self.node.props.name, self.node)

